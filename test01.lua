package.path = package.path .. ";" .. getScriptPath() .. "\\?.lua"
package.cpath = package.cpath .. ";" .. getWorkingFolder() .. "\\?51.dll"


require "QL"
require "settings"

local wlog=nil


	wlog = QTable.new()
 	wlog:AddColumn("date", QTABLE_STRING_TYPE, 20)
  	wlog:AddColumn("message", QTABLE_STRING_TYPE, 100)
	wlog:SetCaption("Log Window")
   	wlog:Show()




function  AddMess(msg)
    if wlog:IsClosed()
    then
		wlog:Show()
	end

	local row=wlog:AddLine()
   	wlog:SetValue(row,"date",os.date())
  	wlog:SetValue(row,"message",msg)

	wlog:Highlight(row,nil,RED,nil,500)
	toLog(log,msg)
end




ds=CreateDataSource(Settings.class_code,Settings.sec_code,  INTERVAL_M1)  
AddMess("Settings > "..table2string(Settings))
AddMess(ds)





